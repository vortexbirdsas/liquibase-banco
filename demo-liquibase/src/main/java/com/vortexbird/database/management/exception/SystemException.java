package com.vortexbird.database.management.exception;

public class SystemException extends VortexbirdException {
	private static final long serialVersionUID = 1L;

	public SystemException(String exception) {
		super(500, exception, null);
	}

	public SystemException(String exception, Exception e) {
		super(500, exception, e);
	}
}
