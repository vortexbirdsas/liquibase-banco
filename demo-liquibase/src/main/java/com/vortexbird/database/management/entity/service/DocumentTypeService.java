package com.vortexbird.database.management.entity.service;

import com.vortexbird.database.management.domain.DocumentType;

/**
 * @author Zathura Code Generator Version 9.0 http://zathuracode.org
 *         www.zathuracode.org
 *
 */
public interface DocumentTypeService extends GenericService<DocumentType, Integer> {
}
