package com.vortexbird.database.management.entity.service;

import com.vortexbird.database.management.domain.RegisteredAccount;

/**
 * @author Zathura Code Generator Version 9.0 http://zathuracode.org
 *         www.zathuracode.org
 *
 */
public interface RegisteredAccountService extends GenericService<RegisteredAccount, Integer> {
}
