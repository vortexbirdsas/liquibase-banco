package com.vortexbird.database.management.mapper;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import com.vortexbird.database.management.domain.RegisteredAccount;
import com.vortexbird.database.management.dto.RegisteredAccountDTO;

/**
 * @author Zathura Code Generator Version 9.0 http://zathuracode.org
 *         www.zathuracode.org
 *
 *         Mapper Build with MapStruct https://mapstruct.org MapStruct is a code
 *         generator that greatly simplifies the implementation of mappings
 *         between Java bean type based on a convention over configuration
 *         approach.
 */
@Mapper
public interface RegisteredAccountMapper {
	@Mapping(source = "account.accoId", target = "accoIdAccount")
	@Mapping(source = "customer.custId", target = "custIdCustomer")
	public RegisteredAccountDTO registeredAccountToRegisteredAccountDTO(RegisteredAccount registeredAccount);

	@Mapping(source = "accoIdAccount", target = "account.accoId")
	@Mapping(source = "custIdCustomer", target = "customer.custId")
	public RegisteredAccount registeredAccountDTOToRegisteredAccount(RegisteredAccountDTO registeredAccountDTO);

	public List<RegisteredAccountDTO> listRegisteredAccountToListRegisteredAccountDTO(
			List<RegisteredAccount> registeredAccounts);

	public List<RegisteredAccount> listRegisteredAccountDTOToListRegisteredAccount(
			List<RegisteredAccountDTO> registeredAccountDTOs);
}
