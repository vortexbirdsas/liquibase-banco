package com.vortexbird.database.management.entity.service;

import com.vortexbird.database.management.domain.UserType;

/**
 * @author Zathura Code Generator Version 9.0 http://zathuracode.org
 *         www.zathuracode.org
 *
 */
public interface UserTypeService extends GenericService<UserType, Integer> {
}
