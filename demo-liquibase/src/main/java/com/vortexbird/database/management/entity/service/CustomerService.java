package com.vortexbird.database.management.entity.service;

import com.vortexbird.database.management.domain.Customer;

/**
 * @author Zathura Code Generator Version 9.0 http://zathuracode.org
 *         www.zathuracode.org
 *
 */
public interface CustomerService extends GenericService<Customer, Integer> {
}
