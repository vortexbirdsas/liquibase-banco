package com.vortexbird.database.management.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.vortexbird.database.management.domain.Customer;
import com.vortexbird.database.management.domain.DocumentType;
import com.vortexbird.database.management.dto.CustomerDTO;
import com.vortexbird.database.management.entity.service.CustomerService;
import com.vortexbird.database.management.exception.UserException;
import com.vortexbird.database.management.exception.VortexbirdException;
import com.vortexbird.database.management.repository.DocumentTypeRepository;

@Service
public class DemoCustomerServiceImpl implements DemoCustomerService {

	@Autowired
	private CustomerService customerService;
	
	@Autowired
	private DocumentTypeRepository documentTypeRepository;
	
	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void saveCustomer(CustomerDTO request) throws VortexbirdException {
		validateCustomer(request);
		
		Customer customer = new Customer();
		customer.setName(request.getName().trim());
		customer.setEmail(request.getEmail().trim());
		customer.setPhone(request.getPhone().trim());
		customer.setAddress(request.getAddress().trim());
		
		Optional<DocumentType> documentTypeOpt = documentTypeRepository.findById(request.getDotyIdDocumentType());
		
		customer.setDocumentType(documentTypeOpt.get());
		customer.setEnable("A");
		
		customerService.save(customer);
		
	}

	public void validateCustomer(CustomerDTO request) throws VortexbirdException {
		if(request == null) {
			throw new UserException("Request empty");
		}
		if(request.getName() == null || request.getName().isBlank()) {
			throw new UserException("Name empty");
		}
		if(request.getEmail() == null || request.getEmail().isBlank()) {
			throw new UserException("Email empty");
		}
		if(request.getPhone() == null || request.getPhone().isBlank()) {
			throw new UserException("Phone empty");
		}
		if(request.getAddress() == null || request.getAddress().isBlank()) {
			throw new UserException("Address empty");
		}
		if(request.getDotyIdDocumentType() == null) {
			throw new UserException("Document type empty");
		}
	}

	@Override
	@Transactional(readOnly = true)
	public CustomerDTO getCustomerById(Integer id) {
		Optional<Customer> customerOpt = customerService.findById(id); 
		if(customerOpt.isEmpty()) {
			return null;
		}
		Customer customer = customerOpt.get();
		CustomerDTO customerDTO = new CustomerDTO();
		customerDTO.setCustId(customer.getCustId());
		customerDTO.setName(customer.getName());
		customerDTO.setEmail(customer.getEmail());
		customerDTO.setPhone(customer.getPhone());
		customerDTO.setAddress(customer.getAddress());
		customerDTO.setDotyIdDocumentType(customer.getDocumentType().getDotyId());
		customerDTO.setEnable(customer.getEnable());
		return customerDTO;
	}
}
