package com.vortexbird.database.management.service;

import com.vortexbird.database.management.dto.CustomerDTO;
import com.vortexbird.database.management.exception.VortexbirdException;

public interface DemoCustomerService {

	public void saveCustomer(CustomerDTO customer) throws VortexbirdException;
	
	public CustomerDTO getCustomerById(Integer id);
	
}
