package com.vortexbird.database.management.entity.service;

import com.vortexbird.database.management.domain.TransactionType;

/**
 * @author Zathura Code Generator Version 9.0 http://zathuracode.org
 *         www.zathuracode.org
 *
 */
public interface TransactionTypeService extends GenericService<TransactionType, Integer> {
}
