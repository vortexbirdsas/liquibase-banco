package com.vortexbird.database.management.mapper;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import com.vortexbird.database.management.domain.Transaction;
import com.vortexbird.database.management.dto.TransactionDTO;

/**
 * @author Zathura Code Generator Version 9.0 http://zathuracode.org
 *         www.zathuracode.org
 *
 *         Mapper Build with MapStruct https://mapstruct.org MapStruct is a code
 *         generator that greatly simplifies the implementation of mappings
 *         between Java bean type based on a convention over configuration
 *         approach.
 */
@Mapper
public interface TransactionMapper {
	@Mapping(source = "account.accoId", target = "accoIdAccount")
	@Mapping(source = "transactionType.trtyId", target = "trtyIdTransactionType")
	@Mapping(source = "users.userEmail", target = "userEmailUsers")
	public TransactionDTO transactionToTransactionDTO(Transaction transaction);

	@Mapping(source = "accoIdAccount", target = "account.accoId")
	@Mapping(source = "trtyIdTransactionType", target = "transactionType.trtyId")
	@Mapping(source = "userEmailUsers", target = "users.userEmail")
	public Transaction transactionDTOToTransaction(TransactionDTO transactionDTO);

	public List<TransactionDTO> listTransactionToListTransactionDTO(List<Transaction> transactions);

	public List<Transaction> listTransactionDTOToListTransaction(List<TransactionDTO> transactionDTOs);
}
