package com.vortexbird.database.management.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vortexbird.database.management.domain.TransactionType;

/**
 * @author Zathura Code Generator Version 9.0 http://zathuracode.org
 *         www.zathuracode.org
 *
 */
public interface TransactionTypeRepository extends JpaRepository<TransactionType, Integer> {
}
