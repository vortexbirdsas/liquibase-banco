package com.vortexbird.database.management.entity.service;

import java.util.List;
import java.util.Optional;

import com.vortexbird.database.management.exception.VortexbirdException;

/**
 * @author Zathura Code Generator Version 9.0 http://zathuracode.org/
 *         www.zathuracode.org
 * 
 */
public interface GenericService<T, ID> {

	public List<T> findAll();

	public Optional<T> findById(ID id);

	public T save(T entity) throws VortexbirdException;

	public T update(T entity) throws VortexbirdException;

	public void delete(T entity) throws VortexbirdException;

	public void deleteById(ID id) throws VortexbirdException;

	public void validate(T entity) throws VortexbirdException;

	public Long count();

}
