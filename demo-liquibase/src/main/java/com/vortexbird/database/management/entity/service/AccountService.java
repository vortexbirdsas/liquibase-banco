package com.vortexbird.database.management.entity.service;

import com.vortexbird.database.management.domain.Account;

/**
 * @author Zathura Code Generator Version 9.0 http://zathuracode.org
 *         www.zathuracode.org
 *
 */
public interface AccountService extends GenericService<Account, String> {
}
