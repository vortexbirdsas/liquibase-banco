package com.vortexbird.database.management.mapper;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import com.vortexbird.database.management.domain.Account;
import com.vortexbird.database.management.dto.AccountDTO;

/**
 * @author Zathura Code Generator Version 9.0 http://zathuracode.org
 *         www.zathuracode.org
 *
 *         Mapper Build with MapStruct https://mapstruct.org MapStruct is a code
 *         generator that greatly simplifies the implementation of mappings
 *         between Java bean type based on a convention over configuration
 *         approach.
 */
@Mapper
public interface AccountMapper {
	@Mapping(source = "customer.custId", target = "custIdCustomer")
	public AccountDTO accountToAccountDTO(Account account);

	@Mapping(source = "custIdCustomer", target = "customer.custId")
	public Account accountDTOToAccount(AccountDTO accountDTO);

	public List<AccountDTO> listAccountToListAccountDTO(List<Account> accounts);

	public List<Account> listAccountDTOToListAccount(List<AccountDTO> accountDTOs);
}
