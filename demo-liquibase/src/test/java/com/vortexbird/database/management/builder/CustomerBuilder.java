package com.vortexbird.database.management.builder;

import java.util.UUID;

import com.vortexbird.database.management.dto.CustomerDTO;

public class CustomerBuilder {

	public static CustomerDTO getValidCustomer() {
		CustomerDTO customer = new CustomerDTO();
		customer.setName("Emilio Rodriguez");
		customer.setEmail("erodriguez@vortexbird.com");
		customer.setPhone("+57" + UUID.randomUUID().toString());
		customer.setAddress("Avenida Siempreviva");
		customer.setDotyIdDocumentType(1);
		return customer;
	}
	
}
