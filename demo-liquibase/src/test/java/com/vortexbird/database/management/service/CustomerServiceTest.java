package com.vortexbird.database.management.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.vortexbird.database.management.builder.CustomerBuilder;
import com.vortexbird.database.management.dto.CustomerDTO;
import com.vortexbird.database.management.exception.UserException;
import com.vortexbird.database.management.exception.VortexbirdException;

@ExtendWith(MockitoExtension.class)
class CustomerServiceTest {
	
	@InjectMocks
	private DemoCustomerServiceImpl demoCustomerService;

	@Test
	void requestEmpty() {
		// Arrange
		CustomerDTO request = null;
		String messageExpected = "Request empty";
		
		// Act
		VortexbirdException exception = assertThrows(UserException.class,
				() -> demoCustomerService.saveCustomer(request));

		
		// Assert
		assertEquals(messageExpected, exception.getException());
	}
	
	@Test
	void requestNameEmpty() {
		// Arrange
		CustomerDTO request = CustomerBuilder.getValidCustomer();
		request.setName(null);
		
		String messageExpected = "Name empty";
		
		// Act
		VortexbirdException exception = assertThrows(UserException.class,
				() -> demoCustomerService.saveCustomer(request));

		
		// Assert
		assertEquals(messageExpected, exception.getException());
	}
	
	@Test
	void requestEmailEmpty() {
		// Arrange
		CustomerDTO request = CustomerBuilder.getValidCustomer();
		request.setEmail(null);
		
		String messageExpected = "Email empty";
		
		// Act
		VortexbirdException exception = assertThrows(UserException.class,
				() -> demoCustomerService.saveCustomer(request));

		
		// Assert
		assertEquals(messageExpected, exception.getException());
	}
	
	@Test
	void requestPhoneEmpty() {
		// Arrange
		CustomerDTO request = CustomerBuilder.getValidCustomer();
		request.setPhone(null);
		
		String messageExpected = "Phone empty";
		
		// Act
		VortexbirdException exception = assertThrows(UserException.class,
				() -> demoCustomerService.saveCustomer(request));

		
		// Assert
		assertEquals(messageExpected, exception.getException());
	}
	
	@Test
	void requestAddressEmpty() {
		// Arrange
		CustomerDTO request = CustomerBuilder.getValidCustomer();
		request.setAddress(null);
		
		String messageExpected = "Address empty";
		
		// Act
		VortexbirdException exception = assertThrows(UserException.class,
				() -> demoCustomerService.saveCustomer(request));

		
		// Assert
		assertEquals(messageExpected, exception.getException());
	}
	
	@Test
	void requestDocumentTypeEmpty() {
		// Arrange
		CustomerDTO request = CustomerBuilder.getValidCustomer();
		request.setDotyIdDocumentType(null);
		
		String messageExpected = "Document type empty";
		
		// Act
		VortexbirdException exception = assertThrows(UserException.class,
				() -> demoCustomerService.saveCustomer(request));

		
		// Assert
		assertEquals(messageExpected, exception.getException());
	}
}