package com.vortexbird.database.management.integration;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.vortexbird.database.management.builder.CustomerBuilder;
import com.vortexbird.database.management.dto.CustomerDTO;
import com.vortexbird.database.management.entity.service.CustomerService;
import com.vortexbird.database.management.exception.VortexbirdException;
import com.vortexbird.database.management.service.DemoCustomerService;

import lombok.extern.log4j.Log4j2;

@SpringBootTest()
@Log4j2
class CustomerServiceIT {

	@Autowired
	private DemoCustomerService demoCustomerService;
	
	@Autowired
	private CustomerService customerService;

	@Test
	void saveCustomer() throws VortexbirdException {
		// Arrange
		CustomerDTO request = CustomerBuilder.getValidCustomer();
		Long customersBefore;
		Long customersAfter;
		// Act
		customersBefore = customerService.count();
		demoCustomerService.saveCustomer(request);
		customersAfter = customerService.count();
		
		log.info(customersBefore);
		assertEquals(customersBefore + 2, customersAfter);
	}

}