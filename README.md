# Liquibase Banco
El proyecto es un laboratorio con el objetivo de capacitar al equipo de desarrollo. El proyecto incluye los archivos para usar liquibase con unas rutas configuradas y apuntando una base de datos postgres.

# Comenzando 🚀
Estas instrucciones te permitirán obtener una copia del proyecto en funcionamiento en tu máquina local para propósitos de desarrollo.

# Pre-requisitos 📋

Para importar el proyecto, necesitas tener instalado Java 17.x o superior y Maven.

El JDK lo descargaremos e instalaremos de la siguiente URL: https://adoptium.net/es/?variant=openjdk16

mvn --version

(En caso de no tener maven descargado o correctamente configurado sigan el "Paso 0").

java -version

# Instalación 🔧
Puede descargar una versión del proyecto en el siguiente repositorio Git:

Si no tienes acceso al repositorio, contáctanos en info@vortexbird.com

#### Paso 0. Instalacion de maven

Vamos a descargar el bin de https://dlcdn.apache.org/maven/maven-3/3.8.5/binaries/apache-maven-3.8.5-bin.zip

Descomprimelo en una ruta de su preferencia en el equipo.

Ahora para windows, se deben configurar las siguientes variables de entorno: 

Nombre de variable: M2_HOME
Valor de variable: {ruta_seleccionada}\apache-maven-3.8.5

Nombre de variable: M2
Valor de variable: %M2_HOME%\bin

#### Paso 1. Clone el proyecto desde el repositorio

git clone https://[user]@bitbucket.org/vortexbirdsas/liquibase-banco.git

#### Paso 2 Importé el proyecto desde su STS 4.14.X

* Seleccione su workspace
* En la ventana de Explorador de proyecto presione click derecho
* Importar como...
* Proyecto maven existente, seleccionan la ruta
* Sí a todo

#### Paso 3 Plugin de lombok instalado

* Descargar lombok de la siguiente URL: https://projectlombok.org/download
* Ejecute el JAR
* Especifique la ruta del STS
* Señale el STS y presione en Install/Update

#### Paso 4 Configurando el proyecto. 

Abra el archivo "liquibase.properties", tiene dos opciones: 
 * Puede configurar una base de datos postgres nueva.
 * Solicitarle al expositor las credenciales

# Empaquetado 📦
Ejecute el empaquetado del proyecto con el siguiente comando:

mvn clean package

# Construido con 🛠️
Menciona las herramientas que utilizaste para crear tu proyecto

Zathura Code - Generador de código

Spring Boot - El framework usado

Spring Data JPA - El framework usado

Maven - Gestor de dependencias

Liquibase - Gestor de base de datos

# Contribuyendo 🖇️
Por favor lee el CONTRIBUTING.md para detalles de nuestro código de conducta, y el proceso para enviarnos pull requests.

# Versionado 📌
Usamos BitBucket para el versionado. Para todas las versiones disponibles, mira los tags en este repositorio.

# Autores ✒️
Menciona a todos aquellos que ayudaron a levantar el proyecto desde sus inicios

Camilo José Delgado Herrera - Arquitecto de Soluciones - [Camilo José Delgado Herrera](https://www.linkedin.com/in/camilojdelgadoh/?originalSubdomain=co "Camilo José Delgado Herrera")

Freddy Sebastián García Victoria - Arquitecto de Soluciones - [Freddy Sebastián García Victoria](http://www.linkedin.com/in/fsgarciavictoria "Freddy Sebastián García Victoria") 


# Expresiones de Gratitud 🎁
Comenta a otros sobre este proyecto 📢
Invita una cerveza 🍺 o un café ☕ a alguien del equipo.

Da las gracias públicamente 🤓.
⌨️ con ❤️ por cdelgado y fgarcia 😊